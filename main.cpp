#include <iostream>
#include <iomanip>
#include <dirent.h>
#include <algorithm>
#include <cstring>
#include <sys/stat.h>
#include <fstream>
#include <unistd.h>

#define PROC_DIR "/proc/"

#define STATUS_FILE "status"
#define EXE_FILE "exe"

#define NAME_FIELD "Name:"
#define PID_FIELD "Pid:"
#define THREADS_FIELD "Threads:"

using std::cout;
using std::endl;
using std::setw;

/**
 * Cтруктура для хранения информации о процессе
 * */
struct ProccessInfo
{
    std::string name;
    int pid;
    int threadsCount;
    std::string pathToExecute;
};

/**
 * Проверка строки src на содержание в ней только цифр
 * */
bool isDigits(const std::string& src)
{
    return std::all_of(src.begin(), src.end(), ::isdigit);
}

/**
 * Получение значения в зависимости от требуемого поля field
 * */
std::string getField(const std::string& src, const std::string& field)
{
    auto pos = src.find(field) + field.size() + 1;
    return src.substr(pos, src.find('\n') - pos);
}

/**
 * Получение необходимой информации о процессе
 * */
ProccessInfo getProcessInfo(const std::string& dirPath)
{
    std::ifstream fin(dirPath + "/" + STATUS_FILE);

    /**
     * Считваем содержимое файла в строку
     * */
    std::string content((std::istreambuf_iterator<char>(fin)),
                        (std::istreambuf_iterator<char>()));
    fin.close();

    ProccessInfo info;

    info.name = getField(content, NAME_FIELD);
    info.pid = std::stoi(getField(content, PID_FIELD));
    info.threadsCount = std::stoi(getField(content, THREADS_FIELD));
    info.pathToExecute = info.name;

    char targetName[PATH_MAX + 1];
    std::string pathName = dirPath + "/" + EXE_FILE;

    /**
     * Считываем символическую ссылку с именем exe и запоминаем ее target путь
     * */
    if (readlink(pathName.c_str(), targetName, PATH_MAX) != -1)
        info.pathToExecute = targetName;

    return info;
}

/**
 * Отображение конечного списка процессов
 * */
void showProcessList(const std::vector<ProccessInfo>& pi)
{
    cout << std::left << setw(18) << "NAME"
         << setw(4) << "PID"
         << std::left << std::setw(10) << "THREADS" << "PATH\n";
    for (auto proc : pi)
    {
        cout << std::left << setw(20) << proc.name
             << setw(8) << proc.pid
             << setw(4) << proc.threadsCount
             << proc.pathToExecute << endl;
    }
    cout << "Process count: " << pi.size();
}


int main()
{
    DIR* dir = opendir(PROC_DIR);

    struct dirent* dit;
    char filePath[NAME_MAX];

    std::vector<ProccessInfo> processes;
    while ((dit = readdir(dir)) != NULL) /** Считываем очередной объект */
    {
        if ((strcmp(dit->d_name, ".") == 0) || (strcmp(dit->d_name, "..") == 0))
            continue;

        struct stat st;

        sprintf(filePath, "%s/%s", PROC_DIR, dit->d_name);
        if (lstat(filePath, &st) != 0)
            continue;

        /**
         * Если объект является директорией и его имя состоит из цифр,
         * то это директория процесса
         * */
        if (S_ISDIR(st.st_mode) && isDigits(dit->d_name))
        {
            ProccessInfo info = getProcessInfo(filePath);
            processes.push_back(info);
        }
    }
    showProcessList(processes);

    return 0;
}